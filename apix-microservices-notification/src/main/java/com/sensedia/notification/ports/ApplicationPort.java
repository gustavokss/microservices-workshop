package com.sensedia.notification.ports;

import com.sensedia.notification.domains.Notification;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public interface ApplicationPort {

    void notify(@Valid @NotNull Notification notification);
}
