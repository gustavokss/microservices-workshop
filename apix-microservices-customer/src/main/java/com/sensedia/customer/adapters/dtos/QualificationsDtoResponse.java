package com.sensedia.customer.adapters.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class QualificationsDtoResponse {

    private String status;
}
